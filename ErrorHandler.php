<?php

/**
 * Class ErrorHandler do przechwytywaia błędów
 */
class ErrorHandler
{
    /**
     * Array z blędami
     * @var array
     */
    private $errors = array();

    /**
     * Walidacja i pushnięcie błędu do arrayki
     * @return string
     */
    public function setError() : string{
        if($this->isError()) {
            array_push($this->errors,"Error: " . $_POST['message'] . " on line " . $_POST['lineNumber'] . " in " . $_POST['url']);
            //np. zapis do bazy
            return "Error caught";
        }else{
            return "No errors to catch";
        }
    }

    /**
     * Odczyt arrayki np. z bazy danych
     * @return array
     */
    public function getErrors(): array{
        return $this->errors;
    }

    /**
     * Dla pewnosci czy przypadkiem to nie inny fetch
     * @return bool
     */
    public function isError(): bool{
        if($_POST['message']) {
            if(!empty($_POST['message'])){
                return true;
            }
            return false;
        }
        return false;
    }
}

//moment kiedy fetch próbuję się skomunikować, do umieszczenia w kontrolerze do którego komunikuje się fetch
$errorHandler = new ErrorHandler();
$errorHandler->setError();

